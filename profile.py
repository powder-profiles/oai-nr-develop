#!/usr/bin/env python

"""


Use this profile to instantiate an experiment using Open Air Interface 5g nr 
(https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/5g-nr-development-and-releases) 
to realize an end-to-end SDR-based mobile network. This profile includes the following resources:

  * SDR UE (d430 + USRP X300) running OAI ('rue1')
  * SDR eNodeB (d430 + USRP x300) running OAI ('enb1')

Powder startup scripts automatically configure OAI for the specific allocated resources.

Instructions:

Be sure to setup your SSH keys as outlined in the manual; it's better
to log in via a real SSH client to the nodes in your experiment.

The Open Air Interface source is located under /opt/oai on the enb1
and rue1 nodes.  It is mounted as a clone of a remote blockstore
(dataset) maintained by Powder.  Feel free to change anything in
here, but be aware that your changes will not persist when your
experiment terminates.


After booting is complete, log onto either the enb1 and rue1
nodes. From there, you will be able to start OAI enb and ue services by running:

on 'enb1'
   
    sudo ip link set mtu 9216 dev ifname # change ifname with the usrp interface
    
    cd /opt/oai/develop-nr/cmake_targets/ran_build/build/
    
    sudo ./nr-softmodem -O /local/repository/etc/gnb.conf

on 'rue1'

    sudo ip link set mtu 9216 dev ifname # change ifname with the usrp interface
    
    cd /opt/oai/develop-nr/cmake_targets/ran_build/build/
    
    sudo ./nr-uesoftmodem --numerology 1 -r 106 --phy-test -C 3510000000 --usrp-args "addr=192.168.30.2"

To enable ue software scope, just add "-d" of following line in file /local/repository/bin/nrue.start.sh:

screen -c $OAIETCDIR/enb.screenrc -L -S ue -h 10000 /bin/bash -c "sudo $UEEXEPATH  --numerology 1 -r 106 --phy-test -C 3510000000  --usrp-args "addr=192.168.30.2" -E -d"



This will stop currently running OAI enb/ue service, start enb/ue service
again in a new screen session, and then show OAI processes on 'enb1'/'rue1'. 
You can detach the sesseion by ctrl-a d. 

After detach, you can pull up and monitor the OAI
processes on the rue1 and enb1 nodes. Execute sudo screen -ls to
see what sessions are available. The commands for controlling services
on these nodes are located in /local/repository/bin.

OAI is a project that is in development. As such, it is not always
stable and there will be times when it gets into a failed state that
it can never recover from. Almost always, you will be able to bring
things back by either rebooting the experiment from the portal or
restarting the services by hand from the command line.

For more detailed information:

  * [Getting Started](https://gitlab.flux.utah.edu/jczhu/OAI-NR/blob/master/README.md)


"""

#
# Standard geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.urn as URN

#
# PhantomNet extensions.
#
import geni.rspec.emulab.pnext as PN

#
# Globals
#
class GLOBALS(object):
    OAI_NR_ENB_DS = "urn:publicid:IDN+emulab.net:powdersandbox+ltdataset+gNB-dev"
    OAI_NR_UE_DS = "urn:publicid:IDN+emulab.net:powdersandbox+ltdataset+nrUE-dev"
    OAI_NR_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:update-nr-image"
    OAI_CONF_SCRIPT = "/usr/bin/sudo /opt/oai/phantomnet/bin/config_oai.pl"
    NUC_HWTYPE = "d740"

def connectOAI_DS(node, type):
    # Create remote read-write clone dataset object bound to OAI dataset
    bs = request.RemoteBlockstore("ds-%s" % node.name, "/opt/oai")
    if type == 1:
	    bs.dataset = GLOBALS.OAI_NR_ENB_DS
    else:
	    bs.dataset = GLOBALS.OAI_NR_UE_DS
    
    bs.rwclone = True

    # Create link from node to OAI dataset rw clone
    node_if = node.addInterface("dsif_%s" % node.name)
    bslink = request.Link("dslink_%s" % node.name)
    bslink.addInterface(node_if)
    bslink.addInterface(bs.interface)
    bslink.vlan_tagging = True
    bslink.best_effort = True

#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Profile parameters.
#
pc.defineParameter("FIXED_UE", "Bind to a specific UE",
                   portal.ParameterType.STRING, "",  advanced=True,
                   longDescription="Input the name of a PhantomNet UE node to allocate (e.g., \'ue1\').  Leave blank to let the mapping algorithm choose.")
pc.defineParameter("FIXED_ENB", "Bind to a specific eNodeB",
                   portal.ParameterType.STRING, "",  advanced=True,
                   longDescription="Input the name of a PhantomNet eNodeB device to allocate (e.g., \'nuc1\').  Leave blank to let the mapping algorithm choose.  If you bind both UE and eNodeB devices, mapping will fail unless there is path between them via the attenuator matrix.")

params = pc.bindParameters()

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pc.makeRequestRSpec()

# Add a node to act as the ADB target host
#adb_t = request.RawPC("adb-tgt")
#adb_t.disk_image = GLOBALS.ADB_IMG

# Add OAI EPC (HSS, MME, SPGW) node.
#epc = request.RawPC("epc")
#epc.disk_image = GLOBALS.OAI_EPC_IMG
#epc.addService(rspec.Execute(shell="sh", command=GLOBALS.OAI_CONF_SCRIPT + " -r EPC"))
#connectOAI_DS(epc)

# Add a NUC eNB node.
enb1 = request.RawPC("enb1")
if params.FIXED_ENB:
    enb1.component_id = params.FIXED_ENB
enb1.hardware_type = GLOBALS.NUC_HWTYPE
enb1.Desire("site-meb", 1.0)
enb1.disk_image = GLOBALS.OAI_NR_IMG
connectOAI_DS(enb1,1)
enb1.addService(rspec.Execute(shell="sh", command=GLOBALS.OAI_CONF_SCRIPT + " -r ENB"))
#enb1_rue1_rf = enb1.addInterface("rue1_rf")
enb1_usrp_if = enb1.addInterface( "usrp_if" )
enb1_usrp_if.addAddress( rspec.IPv4Address( "192.168.30.1", "255.255.255.0" ) )

# Add an OTS (Nexus 5) UE
#rue1 = request.UE("rue1")
#if params.FIXED_UE:
#    rue1.component_id = params.FIXED_UE
#rue1.hardware_type = GLOBALS.UE_HWTYPE
#rue1.disk_image = GLOBALS.UE_IMG
#rue1.adb_target = "adb-tgt"
#rue1_enb1_rf = rue1.addInterface("enb1_rf")

# Create the RF link between the Nexus 5 UE and eNodeB
#rflink2 = request.RFLink("rflink2")
#rflink2.addInterface(enb1_rue1_rf)
#rflink2.addInterface(rue1_enb1_rf)

# Add X300 node.
#usrp_enb = request.RawPC( "usrp_enb" )
usrp_enb = request.RawPC( "usrp_enb", component_id='pnbase2' )
usrp_enb.hardware_type = "sdr"
usrp_enb.disk_image = URN.Image(PN.PNDEFS.PNET_AM, "emulab-ops:GENERICDEV-NOVLANS")
usrp_enb_if = usrp_enb.addInterface( "usrp-nuc" )
usrp_enb_if.addAddress( rspec.IPv4Address( "192.168.30.2", "255.255.255.0" ) )

usrplink_enb = request.Link( "usrp-sdr_enb" )
usrplink_enb.addInterface( enb1_usrp_if )
usrplink_enb.addInterface( usrp_enb_if )

# Add a link connecting the NUC eNB and the OAI EPC node.
#epclink = request.Link("s1-lan")
#epclink.addNode(enb1)
#epclink.addNode(epc)

# Add a NUC eNB node.
rue1 = request.RawPC("rue1")
if params.FIXED_ENB:
    rue1.component_id = params.FIXED_ENB
rue1.hardware_type = GLOBALS.NUC_HWTYPE
rue1.Desire("site-meb", 1.0)
rue1.disk_image = GLOBALS.OAI_NR_IMG
connectOAI_DS(rue1,0)
rue1.addService(rspec.Execute(shell="sh", command=GLOBALS.OAI_CONF_SCRIPT + " -r ENB"))
#enb1_rue1_rf = enb1.addInterface("rue1_rf")
rue1_usrp_if = rue1.addInterface( "usrp_if" )
rue1_usrp_if.addAddress( rspec.IPv4Address( "192.168.30.1", "255.255.255.0" ) )

# Add an OTS (Nexus 5) UE
#rue1 = request.UE("rue1")
#if params.FIXED_UE:
#    rue1.component_id = params.FIXED_UE
#rue1.hardware_type = GLOBALS.UE_HWTYPE
#rue1.disk_image = GLOBALS.UE_IMG
#rue1.adb_target = "adb-tgt"
#rue1_enb1_rf = rue1.addInterface("enb1_rf")

# Create the RF link between the Nexus 5 UE and eNodeB
#rflink2 = request.RFLink("rflink2")
#rflink2.addInterface(enb1_rue1_rf)
#rflink2.addInterface(rue1_enb1_rf)

# Add X300 node.
usrp_ue = request.RawPC( "usrp_ue" )
usrp_ue.hardware_type = "sdr"
usrp_ue.disk_image = URN.Image(PN.PNDEFS.PNET_AM, "emulab-ops:GENERICDEV-NOVLANS")
usrp_ue_if = usrp_ue.addInterface( "usrp-nuc" )
usrp_ue_if.addAddress( rspec.IPv4Address( "192.168.30.2", "255.255.255.0" ) )

usrplink_ue = request.Link( "usrp-sdr_ue" )
usrplink_ue.addInterface( rue1_usrp_if )
usrplink_ue.addInterface( usrp_ue_if )

#
# Print and go!
#
pc.printRequestRSpec(request)
