#!/bin/bash

OAIRANDIR="/opt/oai/develop-nr"
#OAIETCDIR="/local/repository/etc"
UEEXE="nr-uesoftmodem.Rel15"
UEEXEPATH="$OAIRANDIR/targets/bin/$UEEXE"
#ENBCONFPATH="/usr/local/etc/oai/enb.conf"

cd /var/tmp

# Kill off running function.
killall -q $UEEXE
sleep 1

cd $OAIRANDIR

#/bin/bash -c "source $OAIRANDIR/oaienv; $OAIRANDIR/targets/bin/init_nas_nos1 UE"

screen -c $OAIETCDIR/enb.screenrc -L -S ue -h 10000 /bin/bash -c "sudo $UEEXEPATH  --numerology 1 -r 106 --phy-test -C 3510000000  --usrp-args "addr=192.168.30.2" -E"
# Startup function.
#screen -c $OAIETCDIR/enb.screenrc -L -S ue -d -m -h 10000 /bin/bash -c "sudo $ENBEXEPATH -U  -C 2680000000 --ue-txgain 85 --ue-rxgain 90 --ue-scan-carrier -r50"

# Do some cleanup.
screen -wipe >/dev/null 2>&1

exit 0
