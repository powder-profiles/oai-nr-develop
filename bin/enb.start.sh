#!/bin/bash

OAIRANDIR="/opt/oai/openairinterface5g"
OAIETCDIR="/local/repository/etc"
ENBEXE="lte-softmodem-nos1.Rel14"
ENBEXEPATH="$OAIRANDIR/targets/bin/$ENBEXE"
ENBCONFPATH="$OAIETCDIR/enb.conf"

cd /var/tmp

# Kill off running function.
killall -q $ENBEXE
sleep 1

cd $OAIRANDIR

/bin/bash -c "source $OAIRANDIR/oaienv; $OAIRANDIR/targets/bin/init_nas_nos1 eNB"

# Startup function.
screen -c $OAIETCDIR/enb.screenrc -L -S enb  -h 10000 /bin/bash -c "sudo $ENBEXEPATH -O $ENBCONFPATH --phy-test -E"

# Do some cleanup.
screen -wipe >/dev/null 2>&1

exit 0
